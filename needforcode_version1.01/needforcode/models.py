from db import db

# 用户信息
class Admin(db.Model):
    __tablename__ = 'admin'
    admin_id = db.Column(db.Integer(), primary_key=True)  # 主键
    username = db.Column(db.String(20), nullable=False)  # 登录账号名
    password = db.Column(db.String(20), nullable=False)  # 密码

# 用户信息
class User(db.Model):
    __tablename__ = 'user'
    user_id = db.Column(db.Integer(), primary_key=True)  # 主键
    username = db.Column(db.String(20), nullable=False)  # 登录账号名
    password = db.Column(db.String(20), nullable=False)  # 密码
    first_name = db.Column(db.String(15))  # 名
    last_name = db.Column(db.String(15))  #姓
    gender = db.Column(db.String(11), default='male')  # 性别
    phone = db.Column(db.String(20))  # 电话号码
    email = db.Column(db.String(50))  # 邮箱
    profile = db.Column(db.String(50))  # 头像
    hobby = db.Column(db.String(50))  # 头像
    reg_date = db.Column(db.DATETIME)  # 注册日期

# 房产信息
class Property(db.Model):
    __tablname__ = 'property'
    pro_id = db.Column(db.Integer(), primary_key=True)  # 主键
    type = db.Column(db.String(11))  # 房屋类型
    description = db.Column(db.String(255))  # 描述
    address1 = db.Column(db.String(30)) #地址1
    address2 = db.Column(db.String(30))  # 地址2
    zipcode = db.Column(db.Integer()) # 邮编
    suburb = db.Column(db.String(30))  # 区
    country = db.Column(db.String(30))  # 国
    owner = db.Column(db.Integer()) # 房主
    bed = db.Column(db.Integer()) # 床
    bedroom = db.Column(db.Integer())  # 卧室
    bathroom = db.Column(db.Integer()) # 浴室
    parking = db.Column(db.Integer())  # 车位


# 订单
class Order(db.Model):
    __tablname__ = 'order'
    order_id = db.Column(db.Integer(), primary_key=True)  # 主键
    status = db.Column(db.Integer(), nullable=False)  # 房屋状态，0：招租中，1：出租中 3：已租完
    price = db.Column(db.Float(10), nullable=False)  # 价格
    start_date = db.Column(db.DATETIME)  # 入住日期
    end_date = db.Column(db.DATETIME)  # 退房日期
    tanent = db.Column(db.Integer())  # 租客
    onwer = db.Column(db.Integer(), nullable=False)  # 房东

# 纪录
class Log(db.Model):
    __tablename__ = 'log'
    log_id = db.Column(db.Integer(), primary_key=True)  # 主键
    client = db.Column(db.Integer())  # 用户名
    content = db.Column(db.Text)  # 登录账号名
    password = db.Column(db.DATETIME)  # 日志日期
