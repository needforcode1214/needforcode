from flask import Flask,render_template,request,redirect,url_for,session
from sqlalchemy import or_
from functools import wraps
from hashlib import md5
from models import Admin,User,Property,Order,Log
from db import db
import os,config

app = Flask(__name__)
app.config.from_object(config)
db.init_app(app)

#initial Database
#db.create_all(app=app)

@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run(debug = True)
