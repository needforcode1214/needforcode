#encoding: utf-8
import os
DEBUG = True

SECRET_KEY = os.urandom(24)

DIALECT = 'mysql'
DRIVER = 'mysqldb'
USERNAME = 'root'
PASSWORD = 'needforcode'
HOST = 'localhost'
PORT = '3306'
DATABASE = 'rentweb'


SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:needforcode@localhost:3306/rentweb?charset=utf8'
SQLALCHEMY_TRACK_MODIFICATIONS = False